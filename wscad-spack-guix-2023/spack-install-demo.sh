# Spack clone and initial commands
git clone --branch v0.20.1 https://github.com/spack/spack.git
source spack/share/spack/setup-env.sh
spack compiler find
spack compilers
# Spack list
spack list "z*"
# Spack install examples
spack install zlib
spack info zlib
spack install zlib+pic~shared~optimize
spack install zlib@1.2.13+pic~shared~optimize%clang
spack find zlib
spack install zlib+pic~shared~optimize
spack diff /qkdk6s2b /xkwdb6nq
spack spec cbc
spack fetch --dependencies --missing llvm
spack external find openmpi
spack install hwloc^openmpi@4.1.2
# Spack load example
spack load unrar
spack view soft blas-dir openblas
gcc blas-example.c -Lblas-dir/lib -Iblas-dir/include -lopenblas -o ex-openblas
LD_LIBRARY_PATH=blas-dir/lib ./ex-openblas
# Spack env example	
spack env create --dir spack-env-wscad-2023
spack env activate spack-env-wscad-2023
spack add zlib@1.2.3
spack add nano@4.7
spack add vim features=tiny ^ncurses@6.1
spack install
spack env deactivate
# spack uninstall --dependents zlib
