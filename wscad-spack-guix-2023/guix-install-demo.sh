sudo apt install guix
guix install glibc-locales
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
GUIX_PROFILE="$HOME/.guix-profile" source "GUIX_PROFILE/etc/profile"
guix package --search="^ascii*"
guix package --list-available="^ascii"
guix package --list-available="^ascii"
guix install asciidoc
guix package --list-installed
guix pull
guix upgrade
guix remove asciidoc

guix install openblas --profile=$HOME/blas-dir
gcc blas-example.c -Lblas-dir/lib -Iblas-dir/include -lopenblas -o ex-openblas
LD_LIBRARY_PATH=blas-dir/lib ./ex-openblas
guix archive --export openblas gcc > foo.nar
guix pack openblas gcc
