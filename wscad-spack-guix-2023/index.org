#+TITLE: Companion material for "Are you root? Reproducible Experiments in User Space"
#+AUTHOR: Vinícius Garcia Pinto,
#+AUTHOR: Lucas Leandro Nesi,
#+AUTHOR: Lucas Mello Schnorr

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../rethink.css" />
#+OPTIONS: toc:nil num:nil html-style:nil

* Materials
  - [[file:chapter-sbc.pdf][Book chapter]]
  - [[file:presentation-site.pdf][Presentation slides]]
  - VirtualBox VM: https://filesender.rnp.br/?s=download&token=0f4fbb2a-086d-4877-bd21-4d31a3477d87
* Spack files
  - [[file:spack-install-demo.sh][Spack commands example]]
  - [[file:pajeng-package.py][=pajeng= package file]]
    - or the [[https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/pajeng/package.py][official file at the Spack repository]]
  - Environment files: [[file:spack.yaml][spack.yaml]] and [[./spack.lock][spack.lock]]
   
* Guix files
  - [[file:guix-install-demo.sh][Guix commands example]]
  - [[file:kj62mkky4v8z15sdp2fx2fankc11wkfh-openblas-gcc-toolchain-tarball-pack.tar.gz][guix pack example]]
  - [[file:foo.nar][guix archive example]]
  - guix channel URL: https://gitlab.com/exp-hpc/guix-channel-2023-wscad.git          
  - [[file:pajeng.scm][paje.scm]] and [[file:pajeng-master.scm][pajeng-master.scm]]

