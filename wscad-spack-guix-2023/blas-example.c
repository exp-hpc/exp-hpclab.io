// Este codigo de exemplo de uso de BLAS foi gerado pelo Chat GPT

#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>

int main() {
    // Dimensões das matrizes
    int m = 3;  // número de linhas da matriz A
    int n = 2;  // número de colunas da matriz B
    int k = 4;  // número de colunas da matriz A e linhas da matriz B

    // Alocação de memória para as matrizes A, B e C
    double *A = (double *)malloc(m * k * sizeof(double));
    double *B = (double *)malloc(k * n * sizeof(double));
    double *C = (double *)malloc(m * n * sizeof(double));

    // Inicialização das matrizes A e B (aqui você pode preencher com seus próprios valores)
    // Por simplicidade, vamos inicializá-las com valores simples
    for (int i = 0; i < m * k; i++) {
        A[i] = i + 1;
    }
    for (int i = 0; i < k * n; i++) {
        B[i] = (i + 1) * 0.1;
    }

    // Realizando a multiplicação de matrizes usando cblas_dgemm
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A, k, B, n, 0.0, C, n);

    // Imprimindo a matriz resultante C
    printf("Matriz C (resultado da multiplicação):\n");
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            printf("%lf\t", C[i * n + j]);
        }
        printf("\n");
    }

    // Liberando a memória alocada
    free(A);
    free(B);
    free(C);

    return 0;
}
