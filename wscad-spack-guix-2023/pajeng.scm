(use-modules
 (guix packages)
 (guix download)
 (guix build-system cmake)
 (guix licenses)
 (gnu packages boost)
 (gnu packages bison)
 (gnu packages flex)
)
(package
 (name "pajeng")
 (version "1.3.6")
 (source
   (origin
      (method url-fetch)
      (uri
        (string-append
          "https://github.com/schnorr/pajeng/archive/refs/tags/"
          version
          ".tar.gz"))
      (sha256
         (base32
           "1cyhp6lgx7w3qw0pxybj26h4pwfv5rcw5vz8p5zl7imhmszj49qs"
          ))))
 (build-system cmake-build-system)
 (arguments
  `(#:tests? #f
    #:validate-runpath? #f)
 )
 (inputs (list
            boost
            bison
            flex
         ))
 (synopsis
    "PajeNG: library and associated tools for Paje trace files")
 (description
    "PajeNG is a re-implementation of the well-known Paje
    visualization tool for the analysis of execution traces.
    PajeNG comprises the libpaje library, and an auxiliary tool
    called pj_dump to transform Paje trace files to
    Comma-Separated Value (CSV). The space-time visualization
    tool called pajeng had been deprecated (removed from the
    sources) since modern tools do a better job (see
    pj_gantt).")
 (home-page "https://github.com/schnorr/pajeng")
 (license gpl3))
