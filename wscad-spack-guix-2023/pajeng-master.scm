(let ((commit "ca24c95cc5b4e53455058e180f01d5a5febccac6")
      (revision "1"))
(package (inherit pajeng)
  (name "pajeng")
  (version (git-version "1.3.6" revision commit))
   (source
     (origin
       (method git-fetch)
       (uri (git-reference
         (url (string-append
                "https://github.com/schnorr/"
                name))
                   (commit commit)))
       (sha256
         (base32
          "1c1ggfgdl5xxq8jkvf774440j5lyn37n8qll354d4lbqxm81v9av")
        )
      )
    )
   (inputs (modify-inputs (package-inputs pajeng)
                    (prepend fmt)))
  )
)
