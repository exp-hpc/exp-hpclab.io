#+TITLE: Good practices for HPC Experiments
#+AUTHOR: Lucas Mello Schnorr,
#+AUTHOR: Vinícius Garcia Pinto,
#+AUTHOR: Lucas Leandro Nesi

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="rethink.css" />
#+OPTIONS: toc:nil num:nil html-style:nil

* Presentation
This repository contains a series of references, checklists and
details about good practices for computational experiments in High
Performance Computing (HPC). The main idea is that these lists
transform your platform in a more controllable environment to conduct
performance analysis with less statistical variability.

* Short courses and Tutorials
  - Short course at [[https://cradrs.github.io/wscad2023/][WSCAD 2023]] -- "Are you Root? Reproducible Experiments in User Space"
    - Authors: Vinícius Garcia Pinto, Lucas Leandro Nesi and Lucas Mello Schnorr
    - [[./wscad-spack-guix-2023/index.html][Material]]
  - Short course at [[https://web.inf.ufpr.br/erad2022/][ERAD/RS 2022]] -- "Apresentação de Resultados
    Experimentais para Processamento de Alto Desempenho em R"
    - Authors: Vinícius Garcia Pinto, Lucas Leandro Nesi and Lucas Mello Schnorr
    - [[./erad-2022/index.html][Material]]
  - Short course at [[http://labp2d.joinville.udesc.br/erad2021/][ERAD/RS 2021]] -- "Are you root? Experimentos
    Reprodutíveis em Espaço de Usuário"
    - Authors: Jessica Imlau Dagostini, Vinícius Garcia Pinto,Lucas
      Leandro Nesi and Lucas Mello Schnorr
    - [[https://doi.org/10.5753/sbc.6150.4.3][Book chapter]] (pt-br)
    - [[http://comissoes.sbc.org.br/eradrs2021/mc/minicurso3-slides.pdf][Slides]] (pt-br)
  - Short course at [[https://eradsp2020.ncc.unesp.br/][ERAD/SP 2020 | ERAMIA/SP 2020]] -- "Introdução à
    Reprodutibilidade de Experimentos Computacionais de Alto
    Desempenho"
    - Authors: Vinícius Garcia Pinto, Lucas Leandro Nesi and Lucas
      Mello Schnorr
    - [[https://github.com/viniciusvgp/boas-praticas/blob/master/minicurso/2020_ERAD_SP_Controle.pdf][Texto]] (pt-br)
    - [[https://github.com/viniciusvgp/boas-praticas/blob/master/slides/2020_ERAD_SP_Controle_slides.pdf][Slides]] (pt-br)
  - Short course at [[https://erad2020.inf.ufsm.br/][ERAD/RS 2020]] -- "Boas Práticas para Experimentos
    Computacionais de Alto Desempenho"
    - Authors: Vinícius Garcia Pinto, Lucas Leandro Nesi and Lucas
      Mello Schnorr
    - [[https://sol.sbc.org.br/livros/index.php/sbc/catalog/view/44/188/402-1][Book chapter]] (pt-br)
    - [[https://sol.sbc.org.br/livros/index.php/sbc/catalog/view/44/188/403-1][Slides]] (pt-br)
    - [[https://github.com/viniciusvgp/boas-praticas/tree/erad-rs-2020][Tutorial]] (pt-br)
    - [[https://www.youtube.com/watch?v=sey1nYkRH74&feature=youtu.be&t=20422][Video]] (pt-br)
  - Tutorial at [[http://www2.sbc.org.br/cradsp/eradsp/2019/][ERAD/SP 2019]] -- "Boas Práticas para Experimentos" 
    - Authors: Lucas Mello Schnorr and Vinícius Garcia Pinto
    - [[https://github.com/viniciusvgp/boas-praticas/blob/erad-sp-2019/slides/2019_ERAD_RS_Controle_slides.pdf][Slides]] (pt-br)
    - [[https://github.com/viniciusvgp/boas-praticas/tree/erad-sp-2019][Tutorial]] (pt-br)
  - Short course at [[https://www.setrem.com.br/erad2019/][ERAD/RS 2019]] -- "Boas Práticas para Experimentos"
    - Authors: Lucas Mello Schnorr and Vinícius Garcia Pinto
    - [[https://www.setrem.com.br/erad2019/data/pdf/minicursos/mc05.pdf][Text]] (pt-br)
    - [[https://www.setrem.com.br/erad2019/data/slides/minicurso5.pdf][Slides]] (pt-br)
    - [[https://github.com/viniciusvgp/boas-praticas/tree/erad-rs-2019][Tutorial]] (pt-br)

* Checklist
  Please, check [[./checklist.org][this document]].  

* Some reproducibility examples in papers (from us):
- *Analyzing Dynamic Task-Based Applications on Hybrid Platforms: An
  Agile Scripting Approach*, Vinicius Garcia Pinto, Luka Stanisic,
  Arnaud Legrand, Lucas Mello Schnorr, Samuel Thibault, Vincent
  Danjean, in /2016 Third Workshop on Visual Performance Analysis
  (VPA@SC)/, Salt Lake City, UT, 2016, pp. 17-24.
  - [[https://doi.org/10.1109/VPA.2016.008][DOI]] and [[http://perf-ev-runtime.gforge.inria.fr/vpa2016/][Reproducible Paper]]
- *A Visual Performance Analysis Framework for Task-based Parallel
  Applications running on Hybrid Clusters*, Vinicius Garcia Pinto,
  Lucas Mello Schnorr, Luka Stanisic, Arnaud Legrand, Samuel Thibault,
  Vincent Danjean, in /Concurrency and Computation: Practice and
  Experience/, Wiley, 2018, 30 (18), pp.1-31.
  - [[https://dx.doi.org/10.1002/cpe.4472][DOI]], [[https://hal.inria.fr/hal-01616632/][Draft]], and [[https://gitlab.in2p3.fr/schnorr/ccpe2017][Companion website]]
- *Visual Performance Analysis of Memory Behavior in a Task-Based
  Runtime on Hybrid Platforms*, Lucas Leandro Nesi, Samuel Thibault,
  Luka Stanisic and Lucas Mello Schnorr, in /2019 19th IEEE/ACM
  International Symposium on Cluster, Cloud and Grid Computing
  (CCGRID)/, Larnaca, Cyprus, 2019, pp. 142-151.
  - [[https://dx.doi.org/10.1109/CCGRID.2019.00025][DOI]] and [[https://gitlab.com/lnesi/starpu_mem_analysis][Companion website]]


* Authors
  - Lucas Mello Schnorr
  - Vinícius Garcia Pinto
  - Lucas Leandro Nesi
